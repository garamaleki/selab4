from blog.grpc import post_pb2_grpc, post_pb2
from blog.models import Post


def grpc_hook(server):
    post_pb2_grpc.add_PostServiceServicer_to_server(PostService(), server)


class PostService(post_pb2_grpc.PostServiceServicer):
    def GetPost(self, request, context):
        post_id = request.id
        post = Post.objects.get(id=post_id)
        return post_pb2.Post(id=post.id, title=post.title, body=post.body, category=post.category.name)

    def CreatePost(self, request, context):
        title = request.title
        category_id = request.category_id
        body = request.body
        post = Post.objects.create(title=title, body=body, category_id=category_id)
        return post_pb2.Post(id=post.id, title=post.title, body=post.body, category=post.category.name)
