import grpc

from blog.grpc import post_pb2_grpc, post_pb2


def run():
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = post_pb2_grpc.PostServiceStub(channel)
        print(stub.GetPost(post_pb2.GetPostRequest(id=1)))


if __name__ == '__main__':
    run()
