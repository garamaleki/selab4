import os
if os.environ.get('GRPCBLOG', None) == 'production':
    from .production import *
elif os.environ.get('GRPCBLOG', None) == 'dev':
    from .dev import *
else:
    from .local import *